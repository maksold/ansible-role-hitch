# Ansible Role: Hitch

Installs and configures Hitch service on CentOS servers.

## Requirements

This role requires:

- Ansible 2.10+
- RHEL/CentOS 7/8

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    hitch_state: present
    hitch_started: true
    hitch_enabled: true

Choose whether to use an init script or systemd unit configuration to start Hitch when it's installed and/or after a system boot.

    hitch_frontend_host: "*"
    hitch_frontend_port: "443"
    hitch_backend_host: "127.0.0.1"
    hitch_backend_port: "8443"
    hitch_workers: "{{ ansible_processor_cores|default(1) }}"
    hitch_pem_file_path: ""
    hitch_extra_config: ""
    hitch_dhparams_file_path: "/etc/ssl/certs/dhparam.pem"

Configuration variables for /etc/hitch/hitch.conf.

## Dependencies

- [maksold.dhparam](https://gitlab.com/maksold/ansible-role-dhparam)

## License

MIT / BSD

## Author Information

This role was created in 2021 by [Maksim Soldatjonok](https://www.maksold.com/).